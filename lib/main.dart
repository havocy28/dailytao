// import 'dart:ffi';
// TODO setup a cancel notifications function and set color on notifications bar
import 'dart:math';

import 'package:daily_tao/storage_util.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
// var preferences = await SharedPreferences.getInstance();// Save a value
// preferences.setString('value_key', 'hello preferences');// Retrieve value later
// var savedValue = preferences.getString('value_key');

void main() async {
  runApp(MaterialApp(
    home: BannerGalleryWidget(),
  ));
}

class BannerGalleryWidget extends StatefulWidget {
  final int payload;

  BannerGalleryWidget({Key key, this.payload}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return BannerGalleryWidgetState();
  }
}

class BannerGalleryWidgetState extends State<BannerGalleryWidget> {
  // int payload =;
  List data;
  var myData;
  int c;
  String chapter;
  String passage;
  int _page;
  bool notificationsOn;
  final notifications = FlutterLocalNotificationsPlugin();
  final PageController controller =
      PageController(initialPage: 0, keepPage: true);

  Text title;

  // void _controller() async {
  //   await StorageUtil.getInstance();
  //   _page = await StorageUtil.getInt("StoredPage");
  //   PageController _controller = PageController(
  //       initialPage: StorageUtil.getInt("StoredPage"), keepPage: false);
  // }

  @override
  void initState() {
    super.initState();
    _loadBook();

    print("outside result: $_page");

    // if (c == null) {
    //   c = 0;
    // }
    // if (myData == null) {
    //   _loadBook();
    //   _showNotificationMediaStyle();
    //   _page = StorageUtil.getInt("StoredPage");
    // }

    final settingsAndroid = AndroidInitializationSettings('tao_clear');
    // final settingsIOS = IOSInitializationSettings(
    //     onDidReceiveLocalNotification: (id, title, body, payload) =>
    //         onSelectNotification(payload));

    notifications.initialize(InitializationSettings(settingsAndroid, null),
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    setState(() {
      _page = int.parse(payload);
      controller.jumpToPage(_page);
    });
  }

  _onPageViewChange(int page) async {
    // c = page;
    // await StorageUtil.getInstance();
    print('page_chnange before $_page');
    _page = page;
    StorageUtil.putInt("StoredPage", page);

    print('page_change after $_page');

    setState(() {});
  }

  Future<void> _loadPayload() async {
    if (widget.payload != null) {
      await StorageUtil.getInstance();
      await StorageUtil.putInt("StoredPage", widget.payload);
    }
    myData = json.decode(await DefaultAssetBundle.of(context)
        .loadString('assets/json/tao_chapters.json'));
  }

  void _updateNotificationPref() async {
    await StorageUtil.getInstance();
    await StorageUtil.putBool("NotificationsOn", notificationsOn);
  }

  void _getCurrentNotification() async {
    await StorageUtil.getInstance();
    StorageUtil.getBool("NotificationsOn");
  }

  Future _loadBook() async {
    await StorageUtil.getInstance();
    if (widget.payload != null) {
      await StorageUtil.putInt("StoredPage", widget.payload);
    }
    if (StorageUtil.getBool("NotifcationsOn") == null) {
      await StorageUtil.putBool("NotificationsOn", false);
    }
    myData = await json.decode(await DefaultAssetBundle.of(context)
        .loadString('assets/json/tao_chapters.json'));
    setState(() {
      if (!StorageUtil.getBool("NotificationsOn")) {
        // When it is turned on
        title = Text(
          'Switch on Daily Tao Reminder',
          style: GoogleFonts.roboto(
              // style: TextStyle(
              fontWeight: FontWeight.w300,
              fontSize: 15.0,
              color: Colors.grey,
              height: 1.5),
        );
      }

      print('starting $_page');
      _page = StorageUtil.getInt("StoredPage");
      print('after $_page');
      notificationsOn = StorageUtil.getBool("NotifcationsOn");
      myData = myData;
      controller.jumpToPage(_page);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            (() {
              if (myData == null) {
                return 'is null';
              } else {
                // '${model.counter}'
                return myData[_page]["Chapter"].toString();
              }
            }()),
            style: GoogleFonts.roboto(
                // style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 20.0,
                color: Colors.black,
                height: 1.5),
          ),
          // debugging the notifications settings
          // flexibleSpace: FloatingActionButton(
          //     backgroundColor: Colors.pink,
          //     onPressed: () => _showNotificationMediaStyle()),
          centerTitle: true,
          backgroundColor: Colors.brown[50],
          elevation: 0),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.brown[50],
                child: PageView.builder(
                  controller: controller,
                  itemCount: 80,
                  itemBuilder: (context, index) {
                    return new SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: Text(
                          (() {
                            if (myData == null) {
                              return 'is null';
                            } else {
                              return myData[index]["Passage"].toString();
                            }
                          }()),
                          style: GoogleFonts.roboto(
                              // style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 15.0,
                              color: Colors.black,
                              height: 1.5),
                        ),
                      ),
                    );
                  },
                  onPageChanged: _onPageViewChange,
                ),
              ),
            ),
            Card(
              color: Colors.brown[50],
              margin: EdgeInsets.all(0),
              child: SwitchListTile(
                  title: title,
                  activeColor: Colors.brown[50],
                  value: StorageUtil.getBool("NotificationsOn"),
                  onChanged: (bool b) {
                    if (b) {
                      // When it is turned on
                      title = null;

                      // _showNotification();
                      _showNotificationMediaStyle();
                    } else {
                      title = Text(
                        'Switch on Daily Tao Reminder',
                        style: GoogleFonts.roboto(
                            // style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 15.0,
                            color: Colors.grey,
                            height: 1.5),
                      );

                      _cancelNotification();
                    }
                    notificationsOn = !notificationsOn;
                    _updateNotificationPref();
                    setState(() {});
                  }),
            )
          ],
        ),
      ),
    );
  }
}

Future<void> _showNotificationMediaStyle() async {
  var time = Time(10, 00, 0);
  // var scheduledNotificationDateTime = DateTime.now().add(Duration(seconds: 10));
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'media channel id',
    'media channel name',
    'media channel description',
    icon: 'tao_clear',
    sound: RawResourceAndroidNotificationSound('bell'),
    largeIcon: DrawableResourceAndroidBitmap('tao_clear'),
    ongoing: true,
    // styleInformation: MediaStyleInformation(),
  );
  var platformChannelSpecifics =
      NotificationDetails(androidPlatformChannelSpecifics, null);
  await flutterLocalNotificationsPlugin.showDailyAtTime(
      0, 'Your Daily Tao is Ready', '', time, platformChannelSpecifics,
      payload: Random().nextInt(79).toString());
}

Future<void> _showNotification() async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'your channel id', 'your channel name', 'your channel description',
      importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
      0, 'plain title', 'plain body', platformChannelSpecifics,
      payload: Random().nextInt(79).toString());
}

// Random().nextInt(79))

Future<void> _cancelNotification() async {
  await flutterLocalNotificationsPlugin.cancel(0);
}
